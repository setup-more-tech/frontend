import requests
from flask import Flask, make_response, request
import json

app = Flask(__name__)


def parse(name='country-list'):
    r = requests.get('https://datahub.io/core/' + name + '/datapackage.json')
    # print(r.json())
    dict = {}
    dict['id'] = r.json()['id']
    dict['name'] = r.json()['name']
    dict['title'] = r.json()['title']
    dict['data'] = []
    for i in r.json()['resources'][1]['schema']['fields']:
        datadict = {}
        # datadict['description'] = i['description']
        datadict['name'] = i['name']
        datadict['type'] = i['type']
        dict['data'].append(datadict)
    # print(dict)
    return (dict)


def load_from_file():
    f = open('datasets.txt', 'r')
    lines = f.readlines()
    for i in range(len(lines) - 1):  # убираем из всех строк '\n'
        lines[i] = lines[i][:-1]
    return lines


def cookies_write(names):
    res = make_response("Setting a cookie")
    res.set_cookie('names', str(names), max_age=(60 * 60 * 24 * 365 * 2))


def cookies_read():
    data = request.cookies.get('names')
    if (type(data) == type('')):
        for i in ['"', "'", '[', ']', '"', ',']:
            data = data.replace(i, '')
        data = data.split(' ')
        return data
    else:
        return []


@app.route('/api/<new_names>')
def datasets(new_names):
    names = set(load_from_file()) # из файла
    cookies_names = cookies_read() # из кукис

    # добавляем всё в set, чтобы избежать повторов
    for name in cookies_names:
        names.add(name)
    return(new_names)['data']
    for name in (new_names)['data']:
        names.add(name)

    # финал записываем в куки
    cookies_write(names)

    result = {}
    for name in names: # парсим данные по датасету с сайта
        result[name] = parse(name)

    return (result)@app.route('/api/<new_names>')

def datasets(new_names):
    names = set(load_from_file()) # из файла
    cookies_names = cookies_read() # из кукис

    # добавляем всё в set, чтобы избежать повторов
    for name in cookies_names:
        names.add(name)
    for name in json.loads(new_names)['data']:
        names.add(name)

    # финал записываем в куки
    cookies_write(names)

    result = {}
    for name in names: # парсим данные по датасету с сайта
        result[name] = parse(name)

    return (result)

@app.route('/api/')
def datasets_file_only():
    names = set(load_from_file()) # из файла
    cookie = cookies_read()
    result = {}
    for name in names: # парсим данные по датасету с сайта
        result[name] = parse(name)

    return (cookie)


if __name__ == '__main__':
    app.run(debug=True)

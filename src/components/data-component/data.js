import React from "react";
import "./data.css";
import ReactModal from 'react-modal';
import DatasetElement from "../datasetElement-component/datasetElement";


class Data extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      showModal: false,
      data: [],
      data_data: [],
      dataSetKey: 0,
      datainfo: null,
      basketArr: [],
    };

    this.handleOpenModal = this.handleOpenModal.bind(this);
    this.handleCloseModal = this.handleCloseModal.bind(this);
    this.getDataSetInfo = this.getDataSetInfo.bind(this);
    this.sendToBasket = this.sendToBasket.bind(this);
  }

  componentDidMount() {
    fetch("http://127.0.0.1:5000/api/")
      .then(res => res.json())
      .then(
        (result) => {
          this.setState({
            data: result,
            data_data: result.data
          });
          console.log(result);
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          })
        }
      )
  }

  sendToBasket(dI, fI) {
    let dataIndex = dI;
    let fieldIndex = fI;
    let data = this.state.data[dataIndex];
    console.log(data, fieldIndex);
    let basketArr = this.state.basketArr
    basketArr.push({
      data: data,
      fieldIndex: fieldIndex
    })
    console.log(basketArr);
    this.setState({
      basketArr: basketArr,
      showModal: false
    });
    this.props.data(basketArr);
  }

  openBasketModal() {
    let basketArr = this.state.basketArr;
  }

  handleOpenModal(index) {
    this.setState({
      showModal: true,
      dataSetKey: index,
    });
  }

  handleCloseModal() {
    this.setState({ showModal: false });
  }

  getDataSetInfo(index) {
    const { data } = this.state;
    console.log(data[index].data[0]);
    return (data[index].data);
  }

  render() {
    const { data, showModal, dataSetKey } = this.state;
    console.log(data);
    return (
      <datasets-container>
        <datasets-grid>
          {data.map((e, i) => (
            <button key={i} onClick={() => this.handleOpenModal(i)}>
              <div>
                <p>Name</p>
                <p>{e.name}</p>
              </div>
              <div>
                <p>Title</p>
                <p>{e.title}</p>
              </div>
              <div>
                <p>Id</p>
                <p>{e.id}</p>
              </div>
            </button>
          ))}
        </datasets-grid>
        <ReactModal
          isOpen={showModal}
          onRequestClose={this.handleCloseModal}
          shouldCloseOnOverlayClick={true}
          closeTimeoutMS={250}
        >
          <DatasetElement data={dataSetKey} func={this.getDataSetInfo} action={this.sendToBasket} />
        </ReactModal>
      </datasets-container>
    );
  }
}


export default Data;
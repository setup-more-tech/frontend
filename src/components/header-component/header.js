import React from 'react';
import ReactModal from 'react-modal';
import { Button } from 'react-bootstrap';
import Basket from '../basket-component/basket';
import './header.css';

const KEYS_TO_FILTERS = ['user.name', 'subject', 'dest.name']

class Header extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            dataarr: [],
            showModal: false,

        }
        this.handleCloseModal = this.handleCloseModal.bind(this);
        this.handleOpenModal = this.handleOpenModal.bind(this);
    }

    handleCloseModal() {
        this.setState({
            showModal: false
        });
    }

    handleOpenModal() {
        this.setState({
            showModal: true
        });
    }

    render() {
        const { showModal } = this.state;

        return (
            <header>
                <div>
                    SetUp Dataset Browser
                </div>
                <div>
                    <Button variant="light">
                        Authorization
                    </Button>
                    <Button variant="light" onClick={this.handleOpenModal}>
                        Basket
                    </Button>
                </div>
                <ReactModal
                    isOpen={showModal}
                    onRequestClose={this.handleCloseModal}
                    shouldCloseOnOverlayClick={true}
                    closeTimeoutMS={250}
                >
                    <Basket data={this.props.data} />
                </ReactModal>
            </header>
        )
    }
}

export default Header;
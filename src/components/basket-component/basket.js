import React from 'react';
import './basket.css';

class Basket extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            basketArray: []
        }
    }

    render() {
        let basketArray = this.props.data();
        if (basketArray.length !== 0) {
            return (
                <basket-contaoner>
                    {basketArray.map((e, i) => (
                        <div>
                            {e.data.name}
                            {e.data.id}
                        </div>
                    ))}
                </basket-contaoner>
            );
        } else {
            return(
                <basket-container>
                    Пусто!
                </basket-container>
            )
        }
    }
}

export default Basket;
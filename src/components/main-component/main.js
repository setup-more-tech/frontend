import React from "react";
import Header from "../header-component/header";
import Data from "../data-component/data";
import ReactModal from "react-modal"
import "./main.css";

ReactModal.setAppElement('body');

class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            basketData: null
        }
        this.getDataInfo = this.getDataInfo.bind(this);
        this.setDataInfo = this.setDataInfo.bind(this);
    }

    setDataInfo(data) {
        this.setState({
            basketData: data
        })
    }

    getDataInfo() {
        let basketData = this.state.basketData;
        return basketData;
    }

    render() {
        return (
            <main-container>
                <Header data={this.getDataInfo} />
                <Data  data={this.setDataInfo}/>
            </main-container>
        )
    }
}

export default Main;
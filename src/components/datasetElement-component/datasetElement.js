import React from 'react';
import GridTable from '@nadavshaar/react-grid-table';
import './datasetElement.css';

const change = () => {
    console.log('a');
}

const columns1 = [
    {
        id: 1,
        field: 'field',
        label: 'Field',
    },
    {
        id: 2,
        field: 'type',
        label: 'Type',
    },
    {
        id: 3,
        field: 'tags',
        label: 'Tags',
    },
    {
        id: 4,
        field: 'terms',
        label: 'Terms',
    },
    {
        id: 'checkbox',
        pinned: true,
        className: '',
        width: '54px',
        minResizeWidth: 0,
        maxResizeWidth: null,
        resizable: false,
        visible: true,
    }
];

class DatasetElement extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: null,
            rows: [],
            columns: columns1,
            sendTo: [],
            checkArr: [],
            index: null
        }
        this.checkboxHandler = this.checkboxHandler.bind(this);
    }

    componentDidMount() {
        let a = this.props.func(this.props.data);
        console.log(a[0]);
        let rows = []
        for (let i = 0; i < a.length; i++) {
            let row = {
                id: i + 1,
                field: a[i].name,
                type: a[i].type,
                tags: '',
                terms: ''
            }
            rows.push(row);
        }
        console.log(rows);
        this.setState({
            index: this.props.data,
            rows: rows
        })
    }

    checkboxHandler(e) {
        let parent = e.target.parentElement;
        let id = parent.getAttribute('data-row-id');
        let checkArr = this.state.checkArr
        let check = e.target.checked;
        if (check) {
            checkArr.push(id);
        } else {
            let index = checkArr.indexOf(id);
            if (index !== -1) {
                checkArr.splice(index, 1);
            } else {
                return;
            }
        }
        console.log(checkArr);
        this.setState({
            checkArr: checkArr
        });
    }

    render() {
        const { rows, columns, index, checkArr } = this.state;
        return (
            <grid-container>
                <GridTable columns={columns} rows={rows} showRowsInformation={true} isPaginated={false} onChange={(e) => this.checkboxHandler(e)} />
                <div className={".button_div"}>
                    <button onClick={() => this.props.action(index, checkArr)}>Кликать сюда</button>
                </div>
            </grid-container>
        );
    }
}

export default DatasetElement;
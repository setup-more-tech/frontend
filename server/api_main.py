from types import NoneType
import requests
from flask import Flask, session
from flask_session.__init__ import Session
from flask_cors import CORS
import json

app = Flask(__name__)
SESSION_TYPE = 'redis'
app.config.from_object(__name__)
sess = Session(app)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})

def parse(name):
    r = requests.get('https://datahub.io/core/' + name + '/datapackage.json')
    print(r)
    dict = {}
    dict['id'] = r.json()['id']
    dict['name'] = r.json()['name']
    dict['title'] = r.json()['title']
    dict['data'] = []
    for i in r.json()['resources'][1]['schema']['fields']:
        datadict = {}
        # datadict['description'] = i['description']
        datadict['name'] = i['name']
        datadict['type'] = i['type']
        dict['data'].append(datadict)
    # print(dict)
    return (dict)


def load_from_file():
    f = open('datasets.txt', 'r')
    lines = f.readlines()
    for i in range(len(lines) - 1):  # убираем из всех строк '\n'
        lines[i] = lines[i][:-1]
    return lines


# def cookies_write(names):
#     res = make_response("Setting a cookie")
#     res.set_cookie('names', str(names), max_age=60 * 60 * 24 * 365 * 2)


# def cookies_read():
#     data = request.cookies.get('names')
#     print(data)
#     if (type(data) == type('')):
#         for i in ['"', "'", '[', ']', '"', ',']:
#             data = data.replace(i, '')
#         data = data.split(' ')
#         return data
#     else:
#         return []


@app.route('/api/<new_name>')
def datasets(new_name):
    cookies_names = []
    if 'name' in session:
        cookies_names = json.loads(session.get('name'))
    if cookies_names == None:
        cookies_names.append(new_name)
    else:
        if new_name not in cookies_names:
            cookies_names.append(new_name)
    names_json = json.dumps(cookies_names)
    session['name'] = names_json
    return (names_json)

@app.route('/api/')
def datasets_get():
    names = set(load_from_file()) # из файла
    result = []
    for name in names: # парсим данные по датасету с сайта
        result.append(parse(name))

    return (json.dumps(result))


if __name__ == '__main__':
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'

    sess.init_app(app)

    app.run(port=5000, debug=True)
